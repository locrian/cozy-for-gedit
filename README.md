# cozy-for-gedit

Cozy, a soft dark-mode theme for Gedit

<img src="cozy.png" style="max-width: 100%;">

Copyright (I guess) 2021 by Locrian (https://codeberg.org/locrian)

A dark-mode theme with pastellish colors.

I made this for myself when I got frustrated with none of the 
default themes being quite to my liking.

Pull requests will only be accepted if you find a glaring error 
somewhere. Since I don't use Gedit for anything more than HTML, CSS, 
XML, or Markup, I can't be counted on to maintain anything else. Feel 
free to make your own fork if you want something else to be included.